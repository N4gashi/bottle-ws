<?php
class AppUtils
{
    const MAX_WARNING_COUNTER = 10;
    const MAX_WAITING_PHOTO_HOURS = 168; // Une semaine

    public static function isSetAndNotEmpty($var)
    {
        return isset($var) && !empty($var);
    }
} 