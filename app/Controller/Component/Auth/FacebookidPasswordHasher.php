<?php
App::uses('AbstractPasswordHasher', 'Controller/Component/Auth');

class FacebookidPasswordHasher extends AbstractPasswordHasher
{
    public function hash($password)
    {
        return $password;
    }

    public function check($password, $hashedPassword)
    {
        return ($password == $hashedPassword);
    }
}