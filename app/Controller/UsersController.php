<?php

class UsersController extends AppController
{
    public function beforeFilter()
    {
        parent::beforeFilter();
        $this->Auth->allow('signin');
    }

    public function signin()
    {
        if($this->request->is('post'))
        {
            if(array_key_exists('User', $this->request->data))
            {
                $remoteFacebookId = $this->request->data['User']['facebook_id'];
                $this->request->data['User']['last_connection'] = date("Y-m-d H:i:s");

                $userid = $this->User->isUserExists($remoteFacebookId);
                if($userid == 0)
                {

                    $this->User->create();
                    if (!$this->User->save($this->request->data))
                    {
                        throw new InternalErrorException("User cannot be created");
                    }
                }
                /*
                else
                {

                    $this->User->id = $userid;
                    if(!$this->User->save($this->request->data))
                    {
                        throw new InternalErrorException("User cannot be updated");
                    }

                }
                */

                $user = $this->User->getUserInfoByFacebookId($remoteFacebookId);
                $this->set(array($user, '_serialize' => 0));
            }
            else
            {
                // 500 Internal Server Error
                throw new InternalErrorException("User data does not exists");
            }

        }
        else
        {
            // 405 Method Not Allowed
            throw new MethodNotAllowedException();
        }
    }

    public function uploadAndSharePicture()
    {
        // TODO : Not implemented yet

        $filename = null;
        $fileData = $this->request['form']['file'];

        if(!empty($fileData['tmp_name']) && is_uploaded_file($fileData['tmp_name']))
        {
            // Strip path information
            $filename = basename($fileData['name']);

            // TODO : Prendre userid aléatoire, faire le share, sauvegarder, PUIS sauvegarder le fichier

            //move_uploaded_file($fileData['tmp_name'], WWW_ROOT . 'uploads' . DS . $filename);
        }
        else
        {
            throw new InternalErrorException("Upload failed.");
        }

        $this->set(array(new Object(), '_serialize' => 0));
    }

    public function getRandomUserId()
    {
        $allUsers = $this->User->find('all', array('conditions' =>
            array('NOT' => array(
                'User.id' => $this->Auth->user('id')
            ))
        ));

        $userid = array('userid' => $allUsers[mt_rand(0,count($allUsers)-1)]['User']['id']);
        $this->set(array($userid, '_serialize' => 0));
    }

    public function getOwnPictures()
    {

    }

    public function getRecievedPictures()
    {
        debug($this->User->Share->getUserRecievedPictures($this->Auth->user('id')));
    }

    public function getPictureFromOcean()
    {
        $matchingPictures = $this->User->Share->getPicturesFromOcean($this->Auth->user('id'));
        $selectedPicture = $matchingPictures[mt_rand(0,count($matchingPictures)-1)];

        if(!$this->User->Share->addShare($selectedPicture['Photo']['id'], $this->Auth->user('id'), $this->request->data['latitude'], $this->request->data['longitude']))
        {
            throw new InternalErrorException("Bottle has not been shared");
        }
        else
        {
            return $selectedPicture;
        }
    }

    public function editUsername($username)
    {
        if($this->request->is('put'))
        {
            $this->User->id = $this->Auth->user('id');
            $this->User->saveField('username', $username, array('validate' => true));
        }
        else
        {
            // 405 Method Not Allowed
            throw new MethodNotAllowedException();
        }
    }

    public function deleteAccount()
    {
        if($this->request->is('get'))
        {
            // TODO : Supprimer le user et les infos associées (shares, photos, comments)
            //$this->User->deleteAll(array('User.id' => $this->Auth->user('id')), true);
            //$this->Auth->logout();
        }
        else
        {
            // 405 Method Not Allowed
            throw new MethodNotAllowedException();
        }
    }
} 