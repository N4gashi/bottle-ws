<?php

class Share extends AppModel
{

    public $actsAs = array('Containable');
    public $recursive = -1;

    public $hasOne = array(
        'Photo' => array(
            'className' => 'Photo',
            'foreign_key' => 'photo_id'
        ),
        'User' => array(
            'className' => 'User',
            'foreign_key' => 'user_to'
        )
    );

    // TODO : Ajouter les règles de validation de chaque champ

    public function addShare($photoId, $userTo, $latitude, $longitude)
    {
        $now = new DateTime();

        $data = array();
        $data['Share'] = array();
        $data['Share']['photo_id'] = $photoId;
        $data['Share']['user_to'] = $userTo;
        $data['Share']['date_sent'] = $now->format('Y-m-d h:i:s');
        $data['Share']['latitude_to'] = $latitude;
        $data['Share']['longitude_to'] = $longitude;

        return $this->save($data, array(
            'validate' => true,
            'fieldList' => array('photo_id', 'user_to', 'date_sent', 'latitude_to', 'longitude_to')
        ));
    }

    public function getUserRecievedPictures($userid)
    {
       return $this->query('
            SELECT * FROM bottle.photos Photo, bottle.shares Share
			WHERE Photo.id = Share.photo_id
			AND Share.user_to = '.$userid.'
			AND Photo.warning_counter < '.AppUtils::MAX_WARNING_COUNTER.'
			ORDER BY Share.date_sent DESC
        ');
    }

    public function getPicturesFromOcean($userid)
    {
        return $this->query('
            SELECT *
            FROM bottle.photos Photo
            INNER JOIN bottle.shares Share ON Photo.id = Share.photo_id
            WHERE Share.user_to != '.$userid.'
            AND Photo.warning_counter < '.AppUtils::MAX_WARNING_COUNTER.'
            AND Photo.user_id != '.$userid.'
            AND (Share.shared IS NULL OR Share.shared = 0)
            AND (time_to_sec(timediff(NOW(), Share.date_sent)) / 3600) > '.AppUtils::MAX_WAITING_PHOTO_HOURS.'
            ORDER BY Share.date_sent DESC
        ');
    }

} 