<?php

class User extends AppModel
{

    public $displayField = 'username';
    public $actsAs = array('Containable');
    public $recursive = -1;

    public $hasMany = array(
        'Photo' => array(
            'className' => 'Photo',
            'foreign_key' => 'user_id'
        ),
        'Share' => array(
            'className' => 'Share',
            'foreign_key' => 'user_to'
        )
    );

    // TODO : Ajouter les règles de validation de chaque champ

    public function isUserExists($facebookId)
    {
        $userCount = $this->find('first', array(
            'conditions' => array('User.facebook_id' => $facebookId)
        ));

        return ($userCount != null) ? $userCount['User']['id'] : 0;
    }

    public function updateLastVisit($userid)
    {
        $this->id = $userid;
        if($this->id)
            $this->saveField('last_connection', date("Y-m-d H:i:s"));
    }

    public function getUserInfoByFacebookId($facebookid)
    {
        $user = $this->find('first', array('conditions' => array('User.facebook_id' => $facebookid)));
        return $user;
    }


} 