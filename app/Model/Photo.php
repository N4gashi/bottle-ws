<?php

class Photo extends AppModel
{

    public $actsAs = array('Containable');
    public $recursive = -1;

    public $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreign_key' => 'user_id'
        ),
        'Share' => array(
            'className' => 'Share',
            'foreign_key' => 'photo_id'
        )
    );

    // TODO : Ajouter les règles de validation de chaque champ

    public function getUserPictures($userid)
    {

    }

} 